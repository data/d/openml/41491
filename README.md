# OpenML dataset: wq

https://www.openml.org/d/41491

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multivariate regression data set from: https://link.springer.com/article/10.1007%2Fs10994-016-5546-z : The Water Quality dataset (Dzeroski et al. 2000) has 14 target attributes that refer to the relative representation of plant and animal species in Slovenian rivers and 16 input attributes that refer to physical and chemical water quality parameters.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41491) of an [OpenML dataset](https://www.openml.org/d/41491). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41491/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41491/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41491/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

